Tuesday 23 November 2021 - This stanza works with EZproxy instances hosted on oclc.org, third party hosted and self-hosted

AnonymousURL +https://media.digitaltheatreplus.com/drm*
AnonymousURL +https://counter.digitaltheatreplus.com/*
AnonymousURL +https://api.digitaltheatreplus.com/*
Title Digital Theatre Plus
HTTPHeader -request -process Bearer
HTTPHeader -request -process x-dt-auth-token
URL https://edu.digitaltheatreplus.com
Host https://media.digitaltheatreplus.com
Host search.digitaltheatreplus.com
Host analytics.digitaltheatreplus.com
Host https://counter.digitaltheatreplus.com
Host api.digitaltheatreplus.com
Host images.ctfassets.net
Host assets.ctfassets.net
Host d10eeigq6mvk6z.cloudfront.net
Host lic.drmtoday.com
Host cdn.contentful.com
HJ media.digitaltheatreplus.com
HJ analytics.digitaltheatreplus.com
HJ edu.digitaltheatreplus.com
HJ counter.digitaltheatreplus.com
HJ api.digitaltheatreplus.com
DJ digitaltheatreplus.com
AnonymousURL -*
